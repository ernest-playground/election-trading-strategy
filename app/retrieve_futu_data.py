from futu import *

instrument = 'US.SPX'
# instrument = 'US.ESmain'
resolution = 'daily'
start_date = '1992-10-01'
end_date = '2020-10-01'
quote_ctx = OpenQuoteContext(host='127.0.0.1', port=11111)
ret, data, page_req_key = quote_ctx.request_history_kline(code=instrument, start=start_date, end=end_date,
                                                          max_count=None, ktype='K_DAY', autype=AuType.QFQ)

# a = True
#
# while(a):
#     ret_1, data_1, page_req_key = quote_ctx.request_history_kline(code='HK.800000', start=start_date, end=end_date,
#                                                                   ktype='K_1M', page_req_key=page_req_key)
#     data.append(data_1)
#     # print(data_1)
#     print(data)

print(data)
data.to_csv(f'./{instrument}_{resolution}_{start_date}_{end_date}.csv')
quote_ctx.close()
