from datetime import datetime

import yaml
import pandas as pd

from configs import CONFIG_PATH
from datasets import DATASET_PATH
from app.util import check_short_position_profit_or_loss, check_long_position_profit_or_loss


with open(f'{CONFIG_PATH}/election_dates.yml') as file:
    contents = yaml.full_load(file)

election_dates = contents['Dates']
election_dates = [datetime.strptime(election_date, '%d-%m-%Y') for election_date in election_dates]
election_dates.reverse()

df = pd.read_csv(f'{DATASET_PATH}/spy_modified.csv')
df.index = pd.to_datetime(df['Date'], format='%d/%m/%Y')

df['signal'] = pd.Series(index=df.index, dtype=bool)
long_signal, long_success = pd.Series(0, index=df.index), pd.Series(0, index=df.index)
short_signal, short_success = pd.Series(0, index=df.index), pd.Series(0, index=df.index)

number_of_trading_days = 10
ratio = 1

for i in range(len(df)):
    if df.index[i] in election_dates:
        df.loc[df.index[i], 'signal'] = True

for i in range(10, len(df) - 60):
    if df.loc[df.index[i-1], 'signal']:
        if df.loc[df.index[i], 'Close'] >= df.loc[df.index[i-1], 'High']:
            long_signal[i] = 1
            if check_long_position_profit_or_loss(
                    df=df, i=i,
                    stop_loss=min(df.loc[df.index[i], 'Low'], df.loc[df.index[i-1], 'Low']),
                    entry_point=df.loc[df.index[i], 'Close'],
                    profit_level=df.loc[df.index[i], 'Close'] -
                                 min(df.loc[df.index[i], 'Low'], df.loc[df.index[i-1], 'Low']),
                    number_of_trading_days=number_of_trading_days, ratio=ratio):
                long_success[i] = 1

        elif df.loc[df.index[i], 'Close'] <= df.loc[df.index[i-1], 'Low']:
            short_signal[i] = 1
            if check_short_position_profit_or_loss(
                    df=df, i=i,
                    stop_loss=max(df.loc[df.index[i], 'High'], df.loc[df.index[i-1], 'High']),
                    entry_point=df.loc[df.index[i], 'Close'],
                    profit_level=max(df.loc[df.index[i], 'High'], df.loc[df.index[i-1], 'High']) -
                                 df.loc[df.index[i], 'Close'],
                    number_of_trading_days=number_of_trading_days, ratio=ratio):
                short_success[i] = 1
        else:
            continue

print(f'No of trading bars: {number_of_trading_days}')
print(f'Ratio: {ratio}')
print(long_signal[long_signal==True])
print(long_success[long_success==True])
print(short_signal[short_signal==True])
print(short_success[short_success==True])



# def long_at_election_day():
#     pnl = []
#
#     for i in range(len(df)):
#         if df.loc[df.index[i], 'signal']:
#             if df.loc[df.index[i], 'Close'] >= df.loc[df.index[i], 'Open']:
#                 print(f'{df.index[i]} : White.')
#             else:
#                 print(f'{df.index[i]} : Black.')
#
#             pnl.append({'Date': df.index[i], 'Profit': df.loc[df.index[i], 'Close'] / df.loc[df.index[i], 'Open'] - 1})
#
#     profit = sum(i['Profit'] for i in pnl)
#
#     print(pnl)
#     print(profit)
#
#
# def next_day():
#     pnl = []
#
#     for i in range(len(df) - 20):
#         if df.loc[df.index[i], 'signal']:
#             if df.loc[df.index[i], 'Close'] >= df.loc[df.index[i], 'Open']:
#                 print(f'{df.index[i]} : White.')
#
#                 if df.loc[df.index[i + 1], 'Close'] >= df.loc[df.index[i + 1], 'Open']:
#                     print(f'{df.index[i + 1]} : White.')
#                 else:
#                     print(f'{df.index[i + 1]} : Black.')
#
#             else:
#                 print(f'{df.index[i]} : Black.')
#
#                 if df.loc[df.index[i + 1], 'Close'] >= df.loc[df.index[i + 1], 'Open']:
#                     print(f'{df.index[i + 1]} : White.')
#                 else:
#                     print(f'{df.index[i + 1]} : Black.')
#
#             pnl.append({'Date': df.index[i], 'Profit': df.loc[df.index[i], 'Close'] / df.loc[df.index[i], 'Open'] - 1})
#
#     profit = sum(i['Profit'] for i in pnl)
#
#     print(pnl)
#     print(profit)
#
