import yfinance as yf

from datasets import DATASET_PATH


vix = yf.Ticker('^VIX')
spy = yf.Ticker('DJI')
vix.history(period='max').to_csv(f'{DATASET_PATH}/vix.csv')
spy.history(period='max').to_csv(f'{DATASET_PATH}/dji.csv')
