from datetime import datetime, timedelta

import yaml
import pandas as pd

from configs import CONFIG_PATH
from datasets import DATASET_PATH
from app.util import check_short_position_profit_or_loss, check_long_position_profit_or_loss


with open(f'{CONFIG_PATH}/election_dates.yml') as file:
    contents = yaml.full_load(file)

election_dates = contents['Dates']
election_dates = [datetime.strptime(election_date, '%d-%m-%Y') for election_date in election_dates]
election_dates.reverse()

# df = pd.read_csv(f'{DATASET_PATH}/VIX_Daily.csv')
# df.index = pd.to_datetime(df['date'], format='%Y-%m-%d %H:%M:%S')

df = pd.read_csv(f'{DATASET_PATH}/spy_modified.csv')
df.index = pd.to_datetime(df['Date'], format='%d/%m/%Y')
d = df[['Open', 'High', 'Low', 'Close', 'Volume']]
df_weekly = d.resample('W-MON').agg({'Open': 'first', 'High': 'max', 'Low': 'min', 'Close': 'last'})

df_weekly['signal'] = pd.Series(index=df_weekly.index, dtype=bool)

for i in range(len(df_weekly)):
    if (df_weekly.index[i] + timedelta(days=1)) in election_dates:
        df_weekly.loc[df_weekly.index[i], 'signal'] = True

long_signal, long_success = pd.Series(0, index=df_weekly.index), pd.Series(0, index=df_weekly.index)
short_signal, short_success = pd.Series(0, index=df_weekly.index), pd.Series(0, index=df_weekly.index)

trade_records = []


def log_trade_record(date_str, direction, entry_point, stop_loss, target_level, successful):
    return {'Date': date_str,
            'Direction': direction,
            'Entry Point': entry_point,
            'Stop Loss': stop_loss,
            'Target Level': target_level,
            'Success': successful}


for i in range(len(df_weekly)-1):
    if df_weekly.loc[df_weekly.index[i], 'signal']:
        if df_weekly.loc[df_weekly.index[i], 'Close'] > df_weekly.loc[df_weekly.index[i], 'Open']:
            if df_weekly.loc[df_weekly.index[i+1], 'Close'] >= df_weekly.loc[df_weekly.index[i+1], 'Open']:
                trade_record = log_trade_record(
                    date_str=str(df_weekly.index[i+1]),
                    direction='L',
                    entry_point=df_weekly.loc[df_weekly.index[i+1], 'Open'],
                    stop_loss=None,
                    target_level=df_weekly.loc[df_weekly.index[i+1], 'Close'],
                    successful=True)
            else:
                trade_record = log_trade_record(
                    date_str=str(df_weekly.index[i+1]),
                    direction='L',
                    entry_point=df_weekly.loc[df_weekly.index[i+1], 'Open'],
                    stop_loss=None,
                    target_level=df_weekly.loc[df_weekly.index[i+1], 'Close'],
                    successful=False)
            trade_records.append(trade_record)
        elif df_weekly.loc[df_weekly.index[i], 'Close'] < df_weekly.loc[df_weekly.index[i], 'Open']:
            if df_weekly.loc[df_weekly.index[i+1], 'Close'] <= df_weekly.loc[df_weekly.index[i+1], 'Open']:
                trade_record = log_trade_record(
                    date_str=str(df_weekly.index[i + 1]),
                    direction='S',
                    entry_point=df_weekly.loc[df_weekly.index[i + 1], 'Open'],
                    stop_loss=None,
                    target_level=df_weekly.loc[df_weekly.index[i + 1], 'Close'],
                    successful=True)
            else:
                trade_record = log_trade_record(
                    date_str=str(df_weekly.index[i + 1]),
                    direction='S',
                    entry_point=df_weekly.loc[df_weekly.index[i + 1], 'Open'],
                    stop_loss=None,
                    target_level=df_weekly.loc[df_weekly.index[i + 1], 'Close'],
                    successful=False)
            trade_records.append(trade_record)
        else:
            continue
    else:
        continue


